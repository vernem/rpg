extends Area2D

@export var damage := 5
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_area_entered(area):
	if(area.get_owner() is Player):
		var player := area.get_owner() as Player
		if !player.knocked: 
			player.take_damage_knockback(damage, -player.last_dir*TileMovable.TILE_SIZE/2)
		else:
			for direction in TileMovable.inputs.values():
				if !player.will_collide(direction * TileMovable.TILE_SIZE):
					player.take_damage_knockback(damage, direction * TileMovable.TILE_SIZE/2)
