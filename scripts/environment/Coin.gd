extends Area2D

@export var amount := 1
@export var scale_curve : Curve

var time : float
func _ready():
	if(GameState.is_deleted(self)):
		queue_free()

func _on_area_entered(area):
	if(area.get_owner() is Player):
		PlayerState.add_coins(amount);
		QuestSystem.updateQuest(QuestSystem.coin_q)
		GameState.delete_permanently(self)
		
		queue_free();

func _process(delta):
	time += delta
	if(time >= 1.0): time = time - 1.0
	$Sprite2D.scale = Vector2.ONE * scale_curve.sample(time)
