class_name Player
extends TileMovable


@export var damage : int
@export var damage_label : PackedScene

var input_enabled = true
var last_dir = Vector2.ZERO
var last_action = ""



func _ready():
	PlayerState.speed_changed.emit(func(): 
		time_coeficient = PlayerState.time_coeficient
		$MoveTimer.wait_time = PlayerState.time_coeficient / speed) 
	super._ready()
	# align position to the middle of a tile
	if(PlayerState.teleporting):
		position = PlayerState.initial_position
		PlayerState.teleporting=false;
	# set timer interval according to the speed
	_reset_pos()
	$MoveTimer.wait_time = 1.0/speed
	HUD.update_health()

func _unhandled_input(event):
	if input_enabled:
		for action in inputs:
			if event.is_action_pressed(action):
				var dir = inputs[action]
				if move_tile(dir):
					# repeat the action in fixed intervals, if it is still pressed
					last_action = action
					last_dir = dir
					$MoveTimer.start()

func _check_interaction(collider : Object, dir : Vector2):
	if(collider is Enemy):
		collider.take_damage_knockback(damage, dir)


func _knockback(dir : Vector2):
	var timer_interrupted = $MoveTimer.is_stopped()
	$MoveTimer.stop()
	input_enabled = false
	await super._knockback(dir)
	input_enabled = true
	
	if timer_interrupted:
		$MoveTimer.start()

func take_damage_knockback(damage : int, dir : Vector2):
	get_hurt(damage)
	_knockback(dir)

func _on_MoveTimer_timeout():
	if Input.is_action_pressed(last_action):
		if move_tile(last_dir):  # do the same move as the last time
			return
	# reset
	last_action = ""
	last_dir = Vector2.ZERO
	$MoveTimer.stop()

func get_hurt(value : int):
	var label_instance = damage_label.instantiate()
	print("Owner node: " + str(get_owner()))
	get_parent().add_child(label_instance)
	label_instance.position = position
	label_instance.init(value)
	
	PlayerState.decrease_health(value)
	get_viewport().get_camera_2d().shake(4)
	if(PlayerState.health <= 0):
		_die()
	else:
		$Sprite2D.modulate = Color.RED 
		$HurtTimer.start()
		await $HurtTimer.timeout
		$Sprite2D.modulate = Color.WHITE
		
func collect_coins(value):
	PlayerState.add_coins(value)

func _die():
	GameState.reset_game()
