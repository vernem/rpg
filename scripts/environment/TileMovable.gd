# Base class for entity that can move on the map

class_name TileMovable
extends StaticBody2D

@export var speed: float = 4

const TILE_SIZE = 16
var knocked := false
var tween;
var time_coeficient := 1.0

const inputs = {
	"Left": Vector2.LEFT,
	"Right": Vector2.RIGHT,
	"Up": Vector2.UP,
	"Down": Vector2.DOWN
}
signal moved


func _ready():
	$RayCast2D.add_exception($MovementBody)

func _reset_pos():
	position.x = int(position.x / TILE_SIZE) * TILE_SIZE
	position.y = int(position.y / TILE_SIZE) * TILE_SIZE
	position += Vector2.ONE * TILE_SIZE/2

func will_collide(dir : Vector2):
	$RayCast2D.target_position = dir
	$RayCast2D.force_raycast_update()
	
	var collides = $RayCast2D.is_colliding()
	return collides

func _check_interaction(collider : Object, dir : Vector2):
	pass

func _knockback(dir : Vector2):
	if tween != null:
		tween.stop()
	tween = create_tween().set_parallel()
	tween.finished.connect(func(): 
		tween = null
		knocked = false
		_reset_pos()
		)
	for i in range(2,0,-1):
		if !will_collide(dir*i):
			knocked = true
			tween.tween_property(self, "position", dir*i,0.25*time_coeficient).as_relative().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT).finished
			return
	tween.tween_property(self,"position", dir*0.4, 0.1*time_coeficient).as_relative().set_trans(Tween.TRANS_CIRC).set_ease(Tween.EASE_OUT).finished

func move_tile(direction: Vector2):
	if tween != null:
		return
	else:
		tween = create_tween().set_parallel()
		tween.finished.connect(
			func(): 
			tween = null
			$MovementBody.position = Vector2.ZERO			
			_reset_pos()
			)
	if !will_collide(direction * TILE_SIZE):
		$MovementBody.position = Vector2.ONE*direction*TILE_SIZE
		tween.tween_property(self, "position", direction * TILE_SIZE, time_coeficient/speed * 0.9).as_relative().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)		
		moved.emit()
		return true
	var collider = $RayCast2D.get_collider()
	_check_interaction(collider, direction*TILE_SIZE)
	if (collider is TileMovable and collider.knocked):
		$MovementBody.position = Vector2.ONE*direction*TILE_SIZE;
		tween.tween_property(self, "position", direction * TILE_SIZE, time_coeficient/speed *0.9).as_relative().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)		
		moved.emit()
		return true 

	tween = null
	return false
