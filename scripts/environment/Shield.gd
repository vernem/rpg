extends Area2D

signal on_collect

func _ready():
	on_collect.connect(func(): 
		QuestSystem.shield_collected = true
		QuestSystem.updateQuest(QuestSystem.combat_q)
		)

func _on_area_entered(area):
	if(area.get_owner() is Player):
		on_collect.emit()
		GameState.delete_permanently(self)
		queue_free()
