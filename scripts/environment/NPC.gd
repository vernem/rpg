class_name QuestNPC
extends StaticBody2D

enum QuestType { noQuest, coinQuest, combatQuest }

@export_multiline var quest_start_message : String
@export_multiline var quest_wait_message : String
@export_multiline var quest_completed_message : String
@export var quest_type : QuestType

var quest : Quest

var player_nearby := false
var action_active := false

signal on_action


# Called when the node enters the scene tree for the first time.
func _ready():
	match quest_type:
		QuestType.coinQuest:
			quest = QuestSystem.coin_q

			QuestSystem.coin_npc = self
		QuestType.combatQuest:
			quest = QuestSystem.combat_q
	
	if(quest != null):
		if(quest.state == Quest.AVAILABLE):
			on_action.connect(func(): QuestSystem.startQuest(quest))
			quest.giver_text = quest_start_message
			setText(quest.giver_text)
			quest.on_start.connect(func():_on_quest_started())
			
		quest.on_completed.connect(func():_on_quest_completed())
		on_quest_updated()
		quest.on_updated.connect(on_quest_updated)

func setText(text : String):
	$CanvasLayer/TextureRect/Label.text = text

func _on_area_entered(area):
	if(area.get_owner() is Player):
		player_nearby = true
		if(on_action.get_connections().size()>0):
			action_active = true
		$AnimationPlayer.play("appear_anim")
	
func _unhandled_input(event):
	if(event.is_action_pressed("Action") and action_active):
		on_action.emit()

func _on_area_exited(area):
	if(area.get_owner() is Player):
		player_nearby = false
		action_active = false
		$AnimationPlayer.play_backwards("appear_anim")
func _on_quest_started():
	on_action.disconnect(func(): QuestSystem.startQuest(quest))
	action_active = false
	quest.giver_text = quest_wait_message
	setText(quest.giver_text)

func on_quest_updated():
	if(quest.special_action != null):
		if player_nearby: action_active = true
		on_action.connect(func():
			quest.special_action.call()
			quest.special_action = null)
	setText(quest.giver_text)

func _on_quest_completed():
	var conns = on_action.get_connections()
	for conn in conns:
		on_action.disconnect(conn["callable"])	
	
	quest.giver_text = quest_completed_message
	setText(quest.giver_text)
