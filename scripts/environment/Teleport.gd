extends Area2D


@export var next_scene: String = "res://scenes/House.tscn"
@export var message: String = "Press SPACE to enter."
@export var dest_location: Vector2

var is_active = false

func _ready():
	$CanvasLayer/TextureRect/Label.text = message

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		PlayerState.teleporting = true
		PlayerState.initial_position = dest_location
		SceneTransitionSingleton.play_transition(next_scene)

func _on_Teleport_area_entered(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play("appear_anim")
		is_active = true

func _on_Teleport_area_exited(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play_backwards("appear_anim")
		is_active = false
