extends StaticBody2D


@export_multiline var message: String = ""


func _ready():
	$CanvasLayer/TextureRect/Label.text = message

func _on_CloseArea_area_entered(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play("appear_anim")


func _on_CloseArea_area_exited(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play_backwards("appear_anim")
