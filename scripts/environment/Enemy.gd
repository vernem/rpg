class_name Enemy
extends TileMovable

@export var player : Node2D
@export var health := 100
@export var damage := 10

@export var item_drop : PackedScene

func _on_screen_entered():
	$MoveTimer.start()
	
func _on_screen_exited():
	$MoveTimer.stop()	

signal on_death

# Called when the node enters the scene tree for the first time.
func _ready():
	if(GameState.is_deleted(self)):
		queue_free()
		return
		
	PlayerState.speed_changed.connect(func(): 
		time_coeficient = GameState.time_coeficient
		$MoveTimer.wait_time = GameState.time_coeficient / speed
		)
	super._ready()
	_reset_pos()
	# set timer interval according to the speed
	$MoveTimer.wait_time = 1.0/speed
	$HealthBar.max_value = health
	_update_healthbar()
	
func _drop_item():
	for i in range(-1,2):
		for j in range(-1,2):
			var target_vector := Vector2(i,j)
			if(!will_collide(Vector2(i,j)*TILE_SIZE)):
				var instance = item_drop.instantiate()
				instance.position = position + target_vector*TILE_SIZE
				get_parent().add_child(instance)
				return
			
func _die():
	PlayerState.add_kill()
	on_death.emit()
	if(item_drop != null):
		_drop_item()
	GameState.delete_permanently(self)
	knocked = true
	queue_free()

func _on_timeout():
	var direction : Vector2
	var player_pos = player.position
	
	if(abs(position.x - player_pos.x) >= abs(position.y-player_pos.y)):
		if(position.x > player_pos.x): direction = Vector2.LEFT
		else: direction = Vector2.RIGHT
	else:
		if(position.y > player_pos.y): direction = Vector2.UP
		else: direction = Vector2.DOWN
		
	if(direction != null):
		move_tile(direction)

func _check_interaction(collider : Object, dir : Vector2):
	if collider is Player:
		collider.take_damage_knockback(damage, dir)
		
func take_damage_knockback(damage : int, dir : Vector2):
	if(take_damage(damage)):
		_knockback(dir)

func take_damage(damage : int):
	health -= damage
	get_viewport().get_camera_2d().shake(2)
	if(health <= 0):
		_die()
		return false
	_update_healthbar()
	return true

func _update_healthbar():
	
	var tween = create_tween()
	tween.tween_property($HealthBar,'value',health, 0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)

func _knockback(dir : Vector2):
	$MoveTimer.stop()
	await super._knockback(dir)
	$MoveTimer.start()
