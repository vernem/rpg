class_name Quest
extends Node

enum { AVAILABLE, TAKEN, COMPLETED }

var state := AVAILABLE
var giver_text : String
var log_text : String
var special_action


signal on_start
signal on_completed
signal on_updated
