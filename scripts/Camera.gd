extends Camera2D

@export var player : Node2D
@export var speed := 2

var positions = [ Vector2(3,0), Vector2(3,3), Vector2(0,3), Vector2(-3,3), Vector2(-3,0), Vector2(-3,-3), Vector2(0,-3), Vector2(3,-3) ]

# Called when the node enters the scene tree for the first time.
func _ready():
	position = player.position;
	pass # Replace with function body.


func _physics_process(delta):
		position = lerp(position, player.position, speed*delta/GameState.time_coeficient);

func shake(shake_times : int):
	var tween = create_tween()
	
	for i in range(shake_times-1):
		
		tween.tween_property(self, 'offset', positions[randi_range(0,positions.size()-1)], 0.05*GameState.time_coeficient)
	tween.tween_property(self, 'offset', Vector2.ZERO, 0.05*GameState.time_coeficient)
	
