class_name SceneTransition
extends Node

func play_transition(scene : String):
	$AnimationPlayer.play("fade")
	await $AnimationPlayer.animation_finished
	get_tree().change_scene_to_file(scene)
	$AnimationPlayer.play_backwards("fade")

func slow_time_overlay(show : bool):
	if show: $AnimationPlayer.play("slow_time")
	else: $AnimationPlayer.play_backwards("slow_time")
