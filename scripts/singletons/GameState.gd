extends Node

# Used for managing overall game logic i.e. 
# restarting and persistent deletion of objects

var _deleted_objects := Dictionary()
var time_coeficient := 1.0 # > 1 - time runs "slower"

func reset_game():
	QuestSystem.coin_q.state = Quest.AVAILABLE
	QuestSystem.combat_q.state = Quest.AVAILABLE
	_deleted_objects.clear()
	PlayerState.coins = 0
	QuestSystem.shield_collected = false
	QuestSystem.completed_quests = 0
	PlayerState.increase_health(PlayerState.MAX_HEALTH)
	get_tree().reload_current_scene()
	
func is_deleted(obj : Node2D) -> bool:
	return _deleted_objects.has(obj.get_path())	
	
func delete_permanently(obj : Node2D):
	var record = { obj.get_path() : true }
	_deleted_objects.merge(record)

