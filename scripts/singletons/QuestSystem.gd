extends Node


var coin_q := Quest.new()
var combat_q := Quest.new()

@export var coin_npc : QuestNPC

var to_collect := 15
var shield_collected := false
var completed_quests := 0

func _ready():
	coin_q.log_text = "Collect 15 coins in the north-western forest"
	combat_q.log_text = "Take the shield from a bandit in eastern sand pit"


func startQuest(quest : Quest):
	HUD.log_quest(quest)
	quest.state = Quest.TAKEN
	quest.on_start.emit()
	updateQuest(quest)

func updateQuest(quest : Quest):
	if(quest.state == Quest.TAKEN):
		if(quest == coin_q and PlayerState.coins >= to_collect):
				quest.giver_text ="Seems like you brought all the coins necessary. \n\nWould you give them to me ? \n[SPACE - Give coins]"
				quest.log_text = "Go back to the house and hand over the coins"
				quest.special_action = (func(): 
					PlayerState.add_coins(-to_collect)
					completeQuest(quest)
					)
				HUD.log_quest(quest)
		elif(quest == combat_q and shield_collected):
			quest.giver_text = "You got the shield! You are a true hero! \n\n[SPACE - Give shield]"
			quest.log_text = "Go back to the house and return the shield"
			quest.special_action = (func(): completeQuest(quest))
			HUD.log_quest(quest)
	quest.on_updated.emit()


func completeQuest(quest : Quest):
	quest.state = Quest.COMPLETED
	quest.on_completed.emit()
	completed_quests+=1
	HUD.unlog_quest(quest)
