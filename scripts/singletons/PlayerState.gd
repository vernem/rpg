extends Node


const MAX_HEALTH = 100

var health := 100
var coins := 0
var enemies_slain := 0
var initial_position : Vector2
var teleporting := false
var time_coeficient := 1.0



var coin_subtractor : Timer

signal speed_changed

func _ready():
	coin_subtractor = Timer.new()
	add_child(coin_subtractor)
	coin_subtractor.wait_time = 1.0
	coin_subtractor.timeout.connect(func(): 
		coins -=1
		HUD.update_coins_immediate(coins)
		if (coins == 15 and QuestSystem.coin_q.state != Quest.COMPLETED) or coins == 0:
			slow_time(false)
			coin_subtractor.stop()
			coin_subtractor.set_paused(false))

func increase_health(value: int):
	health = clamp(health + value, 0, MAX_HEALTH)
	HUD.update_health()

func decrease_health(value: int):
	health = clamp(health - value, 0, MAX_HEALTH)
	HUD.update_health()
	
func add_coins(value: int):
	HUD.update_coins(coins, coins + value)
	coins += value

func _unhandled_input(event):
	if event.is_action_pressed("TimeManipulation"):
		if time_coeficient != 1.0:
			slow_time(false)
		elif coins > 0 and (QuestSystem.coin_q.state == Quest.COMPLETED or coins > 15):
			slow_time(true)

func slow_time(slowing : bool):
	if slowing:
		time_coeficient = 1.5
		GameState.time_coeficient = 2.0
		if coin_subtractor.is_paused():
			coin_subtractor.set_paused(false)
		else:
			coin_subtractor.start()
	else:
		time_coeficient = 1.0
		GameState.time_coeficient = 1.0
		coin_subtractor.set_paused(true)
	SceneTransitionSingleton.slow_time_overlay(slowing)
	speed_changed.emit()

func add_kill():
	enemies_slain+=1
