extends CanvasLayer
var tween : Tween

var quest_logs := Dictionary()

func log_quest(quest : Quest):
	if(quest_logs.has(quest)): quest_logs[quest] = quest.log_text
	else: quest_logs.merge({quest : quest.log_text})
	
	update_quests()
	
func unlog_quest(quest : Quest):
	if quest_logs.has(quest): quest_logs.erase(quest)
	update_quests()

func update_quests():
	var new_text := ""
	$QuestContainer/QuestDestFadeout.text = $QuestContainer/QuestDescLabel.text
	
	$QuestContainer/QuestDestFadeout.modulate.a = 1
	$QuestContainer/QuestDescLabel.modulate.a = 0

	if quest_logs.size() > 0:
		for log in quest_logs.values():
			new_text += str(log) + "\n\n"
	elif QuestSystem.completed_quests == 2:
		new_text = "You have finished the game woohoo!" 
	else: new_text = "No active quests."
	
	$QuestContainer/QuestDescLabel.text = new_text
	
	var tween = create_tween().set_parallel()
	tween.tween_property($QuestContainer/QuestDestFadeout,"modulate:a",0,2).set_ease(Tween.EASE_OUT)
	tween.tween_property($QuestContainer/QuestDescLabel, "modulate:a", 1,2.0).set_ease(Tween.EASE_OUT)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func update_coins(old_value : int, new_value : int):
	if tween != null and tween.is_running():
		tween.stop()
	tween = create_tween()
	tween.tween_method(
		func(value): $CoinLabel.text = "Coins: " + str(round(value))
		,old_value, new_value, 0.25)

func update_coins_immediate(value : int):
	$CoinLabel.text = "Coins: " + str(value)

func update_health():
	var tween = create_tween().set_parallel()	
	tween.tween_property($HPBar,'value',PlayerState.health, 0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)
