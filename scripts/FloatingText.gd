extends Label


func init(damage : int):
	text = str(damage)
	start()

func start():
	var fade_tween = self.create_tween().set_parallel()
	
	fade_tween.tween_property(self, 'position', Vector2.UP*50,0.75).as_relative().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)
	await fade_tween.tween_property(self, 'modulate:a', 0, 0.75).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN).finished
	queue_free()
